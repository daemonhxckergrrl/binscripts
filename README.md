# binscripts
*a collection of personal scripts*

---

This is a collection of small scripts I've written, to be placed in `~/.local/bin`, that I want in my `$PATH`. Scripts related to environment configuration, window management etc. (such as my rofi interface for screenshots) are in `~/.config/scripts` and either managed via a dotfiles management system or a separate dotscripts repo. I haven't made that decision yet !

## Quickstart
Clone the repo and set it to ignore any binary/executable files (without extension) already in `~/.local/bin`:
```sh
git clone https://gitlab.com/daemonhxckergrrl/binscripts.git
touch .gitignore && find . -executable -type f ! -name "*.*" >>.gitignore
```
This should prevent git trying to track other binaries while also allowing and tracking executable scripts.
